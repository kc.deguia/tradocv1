'use strict';
app.controller('registeredListCtrl',  function ($scope, $q, $http, $uibModal) {

	 var fctry = {
    	$getList : function(){
    		let deferred = $q.defer();
            $http({
                 url: "http://final/v1/tradoc/list",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function (success){
            	deferred.resolve(success.data);
			},function (error){
				deferred.resolve(error);
			});
            return deferred.promise; 
        }
    }

    fctry.$getList().then(function(data){
    	$scope.records = data.records;
    }, function(err){

    });



    // MODAL SAMPLE
   $scope.items = ['item1', 'item2', 'item3'];

        $scope.open = function (size, data) {

            var modalInstance = $uibModal.open({
                templateUrl: 'myModalContent.html',
                controller: function($scope){
                	$scope.data = data;
                },
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };


   
    
});
