'use strict';
// app.config(function($locationProvider) {
//     $locationProvider.html5Mode({
//         enabled: true,
//         requireBase: false
//     });
//     $locationProvider.hashPrefix('');
//
//     // $ocLazyLoad.load([
//     //   '../bower_components/sweetalert/dist/sweetalert.min.js',
//     // ]);
//
//     // $ocLazyLoad.load({
//     //   "name" : "angularMoment",
//     //   "files":["bower_components/angular-moment/angular-moment.min.js"]
//     // });
//
//
// });

/**
* Config for the router
*/
app.config(['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'JS_REQUIRES','$interpolateProvider','$httpProvider','$locationProvider',
function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, jsRequires, $interpolateProvider,$httpProvider,$locationProvider ) {

    app.controller = $controllerProvider.register;
    app.directive = $compileProvider.directive;
    app.filter = $filterProvider.register;
    app.factory = $provide.factory;
    app.service = $provide.service;
    app.constant = $provide.constant;
    app.value = $provide.value;

    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');


    // LAZY MODULES
    $ocLazyLoadProvider.config({
        debug: false,
        events: true,
        modules: jsRequires.modules
    });

    // APPLICATION ROUTES
    // -----------------------------------
    // For any unmatched url, redirect to /app/dashboard
    // $urlRouterProvider.otherwise("/dashboard");
    $urlRouterProvider.otherwise("/registered/list");
    //
    // Set up the states
    $stateProvider
    .state('app', {
        url: "/sids",
        templateUrl: "tpl/sids",
        resolve: loadSequence('modernizr', 'moment', 'angularMoment', 'uiSwitch', 'perfect-scrollbar-plugin', 'toaster', 'ngAside', 'vAccordion', 'sweet-alert', 'chartjs', 'tc.chartjs', 'oitozero.ngSweetAlert', 'chatCtrl', 'truncate', 'htmlToPlaintext', 'angular-notification-icons'),
        abstract: false
    })


    .state ('dashboard', {
        url: '/dashboard',
        templateUrl: 'tpl/dashboard',
        resolve: loadSequence('jquery-sparkline', 'htmlToPlaintext', 'dashboardCtrl'),
        title: 'Dashboard',
        ncyBreadcrumb: {
            label: 'Dashboard'
        }
    })

    // STUDENTS
    .state ('student', {
        url: '/student',
        templateUrl: 'tpl/student',
        title: 'Student',
    })
    .state ('studentAdd', {
        url: '/student/new',
        templateUrl: 'tpl/student/new',
        title: 'Add New Student',
    })


    // USERS
    .state ('user', {
        url: '/user',
        templateUrl: 'tpl/user',
    })

    .state ('userNew', {
        url: '/user/new',
        templateUrl: 'tpl/user/new',
    })

    // REGISTERED_LIST
    .state ('registeredList', {
        url: '/registered/list',
        templateUrl: 'tpl/registers',
        controller: 'registeredListCtrl',
        resolve: loadSequence('registeredListCtrl')

    })


    // ACCOUNT
    .state ('account', {
        url: '/account',
        templateUrl: 'tpl/account',
    })
    .state ('system', {
        url: '/system',
        templateUrl: 'tpl/system',
    })

    .state('login', {
	    url: '/login',
	    template: '<div ui-view class="fade-in-right-big smooth"></div>',
	    // abstract: true
	})


    // $httpProvider.interceptors.push('HcInterceptor');

    // $locationProvider.html5Mode(true);


    // Generates a resolve object previously configured in constant.JS_REQUIRES (config.constant.js)
    function loadSequence() {
        var _args = arguments;
        return {
            deps: ['$ocLazyLoad', '$q',
			function ($ocLL, $q) {
			    var promise = $q.when(1);
			    for (var i = 0, len = _args.length; i < len; i++) {
			        promise = promiseThen(_args[i]);
			    }
			    return promise;

			    function promiseThen(_arg) {
			        if (typeof _arg == 'function')
			            return promise.then(_arg);
			        else
			            return promise.then(function () {
			                var nowLoad = requiredData(_arg);
			                if (!nowLoad)
			                    return $.error('Route resolve: Bad resource name [' + _arg + ']');
			                return $ocLL.load(nowLoad);
			            });
			    }

			    function requiredData(name) {
			        if (jsRequires.modules)
			            for (var m in jsRequires.modules)
			                if (jsRequires.modules[m].name && jsRequires.modules[m].name === name)
			                    return jsRequires.modules[m];
			        return jsRequires.scripts && jsRequires.scripts[name];
			    }
			}]
        };
    }

}]);
