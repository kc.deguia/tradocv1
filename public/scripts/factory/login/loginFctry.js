'use strict';
/**
 * Clip-Two Main Controller
 */
app.factory('loginFctry',  function ($http, $q, Config) {

    return {
        $login : function(data){

            let deferred = $q.defer();
            $http({
                url: Config.ApiURL + "/tpl/test/signin",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config){
                deferred.resolve(data);
            }).error(function (data, status, headers, config){
                deferred.resolve(data);
            });

            return deferred.promise;
        }
    }

});
