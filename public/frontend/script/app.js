var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http,  $q) {

	$scope.register = function(data){

		request.$login(data).then(function(cb){
			if(cb.records.success){
				swal(
				  'Registration successfully submit',
				  '[more info here]',
				  'success'
				);

				$scope.data= {};
				$scope.loginForm.$setPristine();
				
			}
		},function (error){
			console.log(error)
		});
	}


	var request = {
		 $login : function(data){
            let deferred = $q.defer();
            $http({
                url: "http://final/v1/tradoc/register",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).then(function (success){
            	deferred.resolve(success.data);
			},function (error){
				deferred.resolve(error);
			});

            return deferred.promise;
        }
	}


})