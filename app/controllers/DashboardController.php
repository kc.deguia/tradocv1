<?php

use Phalcon\Mvc\View;

class DashboardController extends ControllerBase
{

    public function indexAction(){
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}
