<?php

use Phalcon\Mvc\View;

class PageController extends ControllerBase
{

    public function page_404Action(){
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}
