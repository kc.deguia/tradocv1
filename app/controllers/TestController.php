<?php

use Phalcon\Mvc\View;

class TestController extends ControllerBase
{

    public function indexAction(){
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function actionAction(){
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function paramsAction($param){
        $this->view->param     = $param;
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function signinAction(){

        echo json_encode($this->request->getPost());
        die();
    }


}
