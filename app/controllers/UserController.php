<?php

use Phalcon\Mvc\View;

class UserController extends ControllerBase
{
    public function indexAction(){
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function newAction(){
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}
