<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller{
    public function curl($url) {

        $service_url = $url;
        $curl = curl_init($service_url);
        // curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, '/windows nt 6.2/i');
        $curl_response = curl_exec($curl);
        // die(var_dump($curl_response));
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        return $decoded = json_decode($curl_response);
        // return $decoded = $curl_response;
    }
}
