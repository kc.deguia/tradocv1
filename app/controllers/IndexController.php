<?php

use Phalcon\Mvc\View;

class IndexController extends ControllerBase
{

    public function indexAction(){
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

     public function mainAction(){
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}
