<?php

use Phalcon\Dispatcher;
use Phalcon\Mvc\Dispatcher as MvcDispatcher;
use Phalcon\Events\Event;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Mvc\Dispatcher\Exception as DispatchException;

use Phalcon\DI\FactoryDefault,
	Phalcon\Mvc\View,
	Phalcon\Mvc\Url as UrlResolver,
	Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter,
	Phalcon\Mvc\View\Engine\Volt as VoltEngine,
	Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter,
	Phalcon\Session\Adapter\Files as SessionAdapter;



/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

	/**
	 * Registering a router
	 */
	$di['router'] = function() {

		$router = new \Phalcon\Mvc\Router(false);


		$router->add('/web/:controller/:action/:params', [
			'controller' => 1,
			'action' => 2,
			'params' => 3
		]);

		$router->add('/web/:controller/:action', [
			'controller' => 1,
			'action' => 2,
		]);

		$router->add('/web/:controller', [
			'controller' => 1,
			'action' => 'index'
		]);

		$router->add('/tpl/:controller/:action/:params', [
			'controller' => 1,
			'action' => 2,
			'params' => 3
		]);

		$router->add('/tpl/:controller/:action', [
			'controller' => 1,
			'action' => 2,
		]);

		$router->add('/tpl/:controller', [
			'controller' => 1,
			'action' => 'index'
		]);

		$router->add('/window/:params', [
			'controller' => 'index',
			'action' => 'window'
		]);

		$router->add('/document/:action/:params', [
			'controller' => 'document',
			'action' => 1,
			'params' => 2
		]);

		$router->add('/login', [
			'controller' => 'login',
			'action' => 'index'
		]);


		

		$router->add('/unlock', [
			'controller' => 'unlock',
			'action' => 'index'
		]);

		$router->setDefaults([
			'controller' => 'index',
			'action' => 'index'
			]
		);

		$router->removeExtraSlashes(true);

		return $router;
	};

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function() use ($config) {
	$url = new UrlResolver();
	$url->setBaseUri($config->application->baseUri);
	return $url;
}, true);

/**
 * Setting up the view component
 */
$di->set('view', function() use ($config) {

	$view = new View();

	$view->setViewsDir($config->application->viewsDir);

	$view->setLayoutsDir('main/');
	$view->setTemplateAfter('index');

	$view->registerEngines(array(
		'.volt' => function($view, $di) use ($config) {

			$volt = new VoltEngine($view, $di);

			$volt->setOptions(array(
				'compiledPath' => $config->application->cacheDir,
				'compiledSeparator' => '_',
			));

			return $volt;
		},
		'.phtml' => 'Phalcon\Mvc\View\Engine\Php' // Generate Template files uses PHP itself as the template engine
	));

	return $view;
}, true);


/**
 * 404
 */
 $di->setShared(
     "dispatcher",
     function () {
         // Create an EventsManager
         $eventsManager = new EventsManager();

         // Attach a listener
         $eventsManager->attach(
             "dispatch:beforeException",
             function (Event $event, $dispatcher, Exception $exception) {
                 // Handle 404 exceptions
                 if ($exception instanceof DispatchException) {
                     $dispatcher->forward(
                         [
                             "controller" => "page",
                             "action"     => "page_404",
                         ]
                     );

                     return false;
                 }

                 // Alternative way, controller or action doesn't exist
                 switch ($exception->getCode()) {
                     case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                     case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                         $dispatcher->forward(
                             [
                                 "controller" => "page",
                                 "action"     => "page_404",
                             ]
                         );

                         return false;
                 }
             }
         );

         $dispatcher = new MvcDispatcher();

         // Bind the EventsManager to the dispatcher
         $dispatcher->setEventsManager($eventsManager);

         return $dispatcher;
     }
 );

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function() use ($config) {
	return new DbAdapter(array(
		'host' => $config->database->host,
		'username' => $config->database->username,
		'password' => $config->database->password,
		'dbname' => $config->database->dbname
	));
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function() use ($config) {
	return new MetaDataAdapter();
});

/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function() {
	$session = new SessionAdapter();
	$session->start();
	return $session;
});

$config = include "../app/config/config.php";
$di->set('config', function () use ($config) {
    return $config;
});
