<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */

$loader->registerNamespaces(
    [
       "Services"    => __DIR__ . "/../services/"
    ]
);

$loader->registerDirs(
	array(
		$config->application->controllersDir,
		$config->application->modelsDir
	)
)->register();
