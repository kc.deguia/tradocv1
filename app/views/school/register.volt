<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TRADOC</title>

    <!-- Bootstrap core CSS -->
    <link href="/../frontend/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/../frontend/css/full-slider.css" rel="stylesheet">

    <link rel="stylesheet" href="/../bower_components/sweetalert2/dist/sweetalert2.min.css">

  </head>

  <body  ng-app="myApp" >

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">TRADOC</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item ">
              <a class="nav-link" href="../../web/school">Home
               
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="#">Register</a>
               <span class="sr-only">(current)</span>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header>
      <h1>Register<h1>
    </header>

    <!-- Page Content -->
    <section class="py-5" ng-controller="myCtrl">
      <div class="container">
        <h1>Register</h1>
      </div>
      <div class="container">
        <form class="form-login" name="loginForm" id="Login" novalidate ng-submit="register(data);">

          <div class="row">
            <div class="col col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">First Name</label>
                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.fname" aria-describedby="emailHelp" placeholder="First Name...">
                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Middle Name</label>
                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.mname" aria-describedby="emailHelp" placeholder="Middle Name...">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Last Name</label>
                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.lname" aria-describedby="emailHelp" placeholder="Last Name...">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Rank</label>
                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.rank" aria-describedby="emailHelp" placeholder="Rank...">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">AFPSN</label>
                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.afpSn" aria-describedby="emailHelp" placeholder="AFPSN...">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">AFP ID Nr.</label>
                <input type="text" class="form-control" id="exampleInputEmail1"  ng-model="data.afpIdno" aria-describedby="emailHelp" placeholder="AFP ID Nr...">
              </div>
            </div>

            <div class="col col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Date of Birth</label>
                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.dateOfBirth"  aria-describedby="emailHelp" placeholder="Date of Birth...">
                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Place of Birth</label>
                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.placeOfBirth" aria-describedby="emailHelp" placeholder="Place of Birth...">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Religion</label>
                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.religion" aria-describedby="emailHelp" placeholder="Religion...">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Contact No.</label>
                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.contact" aria-describedby="emailHelp" placeholder="Contact No...">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">E-Mail Address</label>
                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.email" aria-describedby="emailHelp" placeholder="E-Mail Address...">
              </div>
            </div>

          </div>

          
          <div class="form-check">
            <label class="form-check-label">
              <input type="checkbox" class="form-check-input" ng-model="data.declareInfo">
              I hereby declare that all the information given above is true
            </label>
          </div>
          <button type="submit" class="btn btn-primary">Register</button>




        </form>
      </div>
    </section>

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Tradoc 2017</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="/../frontend/vendor/jquery/jquery.min.js"></script>
    <script src="/../frontend/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


    <script src="/../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/../bower_components/angular/angular.min.js"></script>
    <script src="/../bower_components/ngSweetAlert/SweetAlert.min.js"></script>
    <script src="/../bower_components/sweetalert2/dist/sweetalert2.min.js"></script>

    <script src="/../frontend/script/app.js"></script>

  </body>

</html>
