<!DOCTYPE html>
<html lang="en" data-ng-app="clipApp">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="description" content="{[{app.description}]}">
		<meta name="keywords" content="app, responsive, angular, bootstrap, dashboard, admin">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta name="HandheldFriendly" content="true" />
		<meta name="apple-touch-fullscreen" content="yes" />
		<title data-ng-bind="pageTitle()">Clip-Two - Angular Bootstrap Admin Template</title>
		<!-- Google fonts -->
		<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
		<!-- Bootstrap -->
		<link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
		<!-- Themify Icons -->
		<link rel="stylesheet" href="../bower_components/themify-icons/css/themify-icons.css">
		<!-- Loading Bar -->
		<link rel="stylesheet" href="../bower_components/angular-loading-bar/build/loading-bar.min.css">
		<!-- Animate Css -->
		<link rel="stylesheet" href="../bower_components/animate.css/animate.min.css">
		<!-- Clip-Two CSS -->
		<link rel="stylesheet" href="assets/css/styles.css">
		<link rel="stylesheet" href="assets/css/plugins.css">
		<!-- Clip-Two Theme -->
		<link rel="stylesheet" data-ng-href="assets/css/themes/{[{ app.layout.theme }]}.css" />

		{#-- INITTIAL_LOADED_MODULE #}
		{# //*** jQuery Pluginss #}
		<link rel="stylesheet" data-ng-href="bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
		<link rel="stylesheet" data-ng-href="bower_components/sweetalert/dist/sweetalert.css" />
		{# //*** angularJS Moduless #}
		<link rel="stylesheet" data-ng-href="bower_components/angular-ui-switch/angular-ui-switch.min.css" />
		<link rel="stylesheet" data-ng-href="bower_components/AngularJS-Toaster/toaster.css" />
		<link rel="stylesheet" data-ng-href="bower_components/angular-aside/dist/css/angular-aside.min.css" />
		<link rel="stylesheet" data-ng-href="bower_components/v-accordion/dist/v-accordion.min.css" />
		<link rel="stylesheet" data-ng-href="bower_components/angular-notification-icons/dist/angular-notification-icons.min.css" />

	</head>
	<body ng-controller="loginCtrl">

		<div  ng-class="{'app-mobile' : app.isMobile, 'app-navbar-fixed' : app.layout.isNavbarFixed, 'app-sidebar-fixed' : app.layout.isSidebarFixed, 'app-sidebar-closed':app.layout.isSidebarClosed, 'app-footer-fixed':app.layout.isFooterFixed}">

			<div class="fade-in-right-big smooth">

			    <!-- start: LOGIN -->

			    <div ng-class="{'app-navbar-fixed': app.layout.isNavbarFixed == false}"></div>
			    <div class="row">
			        <div class="main-login col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			            <div class="logo">
			                <img ng-src="{[{app.layout.logo}]}" alt="{[{app.name}]}" />
			            </div>
			            <!-- start: LOGIN BOX -->
			            <div class="box-login">
			                <form class="form-login" name="Login" id="Login" novalidate ng-submit="login(data);" >
			                    <fieldset>
			                        <legend>
			                            Sign in to your account
			                        </legend>
			                        <p>
			                            Please enter your name and password to log in.
			                        </p>
			                        <div class="form-group">
			                            <span class="input-icon">
			                                <input type="text" class="form-control" ng-model="data.username" name="username" placeholder="Username">
			                                <i class="fa fa-user"></i>
			                            </span>
			                        </div>
			                        <div class="form-group form-actions">
			                            <span class="input-icon">
			                                <input type="password" class="form-control password" ng-model="data.password" name="password" placeholder="Password">
			                                <i class="fa fa-lock"></i>
			                                <a class="forgot" ui-sref="login.forgot">
			                                    I forgot my password
			                                </a>
			                            </span>
			                        </div>
			                        <div class="form-actions">
			                            <div class="checkbox clip-check check-primary">
			                                <input type="checkbox" id="remember" value="1">
			                                <label for="remember">
			                                    Keep me signed in
			                                </label>
			                            </div>
			                            <button type="submit" class="btn btn-primary pull-right" >
			                                Login <i class="fa fa-arrow-circle-right"></i>
			                            </button>
			                        </div>
			                        <div class="new-account">
			                            Don't have an account yet?
			                            <a ui-sref="login.registration">
			                                Create an account
			                            </a>
			                        </div>
			                    </fieldset>
			                </form>
			                <!-- start: COPYRIGHT -->
			                <div class="copyright">
			                    {[{app.year}]} &copy; {[{ app.name }]} by {[{ app.author }]}.
			                </div>
			                <!-- end: COPYRIGHT -->
			            </div>
			            <!-- end: LOGIN BOX -->
			        </div>
			    </div>
			    <!-- end: LOGIN -->


			</div>

		</div>
		

		<!-- jQuery -->
		<script src="../bower_components/jquery/dist/jquery.min.js"></script>
		<!-- Fastclick -->
		<script src="../bower_components/fastclick/lib/fastclick.js"></script>
		<!-- Angular -->
		<script src="../bower_components/angular/angular.min.js"></script>
		<script src="../bower_components/angular-cookies/angular-cookies.min.js"></script>
		<script src="../bower_components/angular-animate/angular-animate.min.js"></script>
		<script src="../bower_components/angular-touch/angular-touch.min.js"></script>
		<script src="../bower_components/angular-sanitize/angular-sanitize.min.js"></script>
		<script src="../bower_components/angular-ui-router/release/angular-ui-router.js"></script>
		<!-- Angular storage -->
		<script src="../bower_components/ngstorage/ngStorage.min.js"></script>
		<!-- Angular Translate -->
		<script src="../bower_components/angular-translate/angular-translate.js"></script>
		<script src="../bower_components/angular-translate-loader-url/angular-translate-loader-url.js"></script>
		<script src="../bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js"></script>
		<script src="../bower_components/angular-translate-storage-local/angular-translate-storage-local.js"></script>
		<script src="../bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.js"></script>
		<!-- oclazyload -->
		<script src="../bower_components/oclazyload/dist/ocLazyLoad.min.js"></script>
		<!-- breadcrumb -->
		<script src="../bower_components/angular-breadcrumb/dist/angular-breadcrumb.min.js"></script>
		<!-- UI Bootstrap -->
		<script src="../bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
		<!-- Loading Bar -->
		<script src="../bower_components/angular-loading-bar/build/loading-bar.min.js"></script>
		<!-- Angular Scroll -->
		<script src="../bower_components/angular-scroll/angular-scroll.min.js"></script>

		{# -- INITTIAL_LOADED_MODULE  #}
		{# //*** Javascript Plugins #}
		<script src="bower_components/modernizr/modernizr.js"></script>
		<script src="bower_components/moment/min/moment.min.js"></script>
		{# //*** jQuery Plugins #}
		<script src="bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
		{#<script src="bower_components/sweetalert/dist/sweetalert.min.js"></script>#}
		<script src="bower_components/chart.js/dist/Chart.min.js"></script>
		{# *** Filters#}
		<!-- <script src="assets/js/filters/htmlToPlaintext.js"></script> -->
		{# //*** angularJS Moduless #}
		<script src="bower_components/angular-moment/angular-moment.min.js"></script>
		<script src="bower_components/angular-ui-switch/angular-ui-switch.min.js"></script>
		<script src="bower_components/AngularJS-Toaster/toaster.js"></script>
		<script src="bower_components/angular-aside/dist/js/angular-aside.min.js"></script>
		<script src="bower_components/v-accordion/dist/v-accordion.min.js"></script>
		<script src="bower_components/tc-angular-chartjs/dist/tc-angular-chartjs.min.js"></script>
		<script src="bower_components/ngSweetAlert/SweetAlert.min.js"></script>
		<script src="bower_components/angular-truncate/src/truncate.js"></script>
		<script src="bower_components/angular-notification-icons/dist/angular-notification-icons.min.js"></script>


		<!-- Clip-Two Scripts -->
		<script src="assets/js/app.js"></script>
		<script src="assets/js/main.js"></script>
		<script src="assets/js/config.constant.js"></script>
		<script src="scripts/fe.router.js"></script>
		<script src="scripts/config.js"></script>
		<!-- Clip-Two Directives -->

		<script src="scripts/controller/login/loginCtrl.js"></script>
		<script src="scripts/factory/login/loginFctry.js"></script>
	

	</body>
</html>
