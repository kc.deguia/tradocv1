
<!-- start: FOURTH SECTION -->
<div class="container-fluid container-fullw bg-white">
	<div class="row">
		<div class="col-xs-12 col-sm-4">
			<h1>Registered List</h1>
		</div>

	</div>
</div>
<!-- end: FOURTH SECTION -->



<div class="container-fluid container-fullw bg-white">
	<div class="row">
		<div class="col-md-12">
			<h5 class="over-title margin-bottom-15">Table <span class="text-bold">List</span></h5>

			<table class="table table-hover" id="sample-table-1">
				<thead>
					<tr>
						<th class="center">Name</th>
						<th>Rank</th>
						<th class="hidden-xs">AFPSN</th>
						<th>AFP ID Nr.</th>
						<th class="hidden-xs">Contact No.</th>
						<th class="hidden-xs">E-Mail</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="list in records">
						<td >{[{ list.fname}]} {[{ list.lname}]}</td>
						<td class="hidden-xs"> {[{ list.rank}]}</td>
						<td>{[{ list.afpSn}]}</td>
						<td>{[{ list.afpIdno}]}</td>
						<td>{[{ list.contact}]}</td>
						<td>{[{ list.email}]}</td>

						<td class="center">
							<div class="visible-md visible-lg hidden-sm hidden-xs">
								<!-- <a href="#" class="btn btn-transparent btn-xs" tooltip-placement="top" uib-tooltip="Edit"><i class="fa fa-pencil"></i></a> -->
								<a ng-click="open('lg', list)" href="#" class="btn btn-transparent btn-xs tooltips" tooltip-placement="top" uib-tooltip="More Info"><i class="fa fa-share"></i></a>
								<!-- <a href="#" class="btn btn-transparent btn-xs tooltips" tooltip-placement="top" uib-tooltip="Remove"><i class="fa fa-times fa fa-white"></i></a> -->
							</div>
							<div class="visible-xs visible-sm hidden-md hidden-lg">
								<div class="btn-group" uib-dropdown is-open="status.isopen">
									<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" uib-dropdown-toggle>
										<i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
									</button>
									<ul class="dropdown-menu pull-right dropdown-light" role="menu">
										<li>
											<a href="#">
												more
											</a>
										</li>
										<!-- <li>
											<a href="#">
												Share
											</a>
										</li>
										<li>
											<a href="#">
												Remove
											</a>
										</li> -->
									</ul>
								</div>
							</div>
						</td>
					</tr>
					
				</tbody>
			</table>
		</div>
	</div>
</div>


<script type="text/ng-template" id="myModalContent.html">
	<div class="modal-header">
		<h4 class="modal-title">Registrant Information</h4>
	</div>
	<div class="modal-body">
		<form class="form-login" name="loginForm" id="Login" novalidate ng-submit="register(data);">

          	<fieldset ng-disabled="true">
          		
          		<div class="row">
	            <div class="col col-lg-6">
	              <div class="form-group">
	                <label for="exampleInputEmail1">First Name</label>
	                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.fname" aria-describedby="emailHelp" placeholder="First Name...">
	                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
	              </div>
	              <div class="form-group">
	                <label for="exampleInputEmail1">Middle Name</label>
	                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.mname" aria-describedby="emailHelp" placeholder="Middle Name...">
	              </div>
	              <div class="form-group">
	                <label for="exampleInputEmail1">Last Name</label>
	                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.lname" aria-describedby="emailHelp" placeholder="Last Name...">
	              </div>
	              <div class="form-group">
	                <label for="exampleInputEmail1">Rank</label>
	                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.rank" aria-describedby="emailHelp" placeholder="Rank...">
	              </div>
	              <div class="form-group">
	                <label for="exampleInputEmail1">AFPSN</label>
	                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.afpSn" aria-describedby="emailHelp" placeholder="AFPSN...">
	              </div>
	              <div class="form-group">
	                <label for="exampleInputEmail1">AFP ID Nr.</label>
	                <input type="text" class="form-control" id="exampleInputEmail1"  ng-model="data.afpIdno" aria-describedby="emailHelp" placeholder="AFP ID Nr...">
	              </div>
	            </div>

	            <div class="col col-lg-6">
	              <div class="form-group">
	                <label for="exampleInputEmail1">Date of Birth</label>
	                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.dateOfBirth"  aria-describedby="emailHelp" placeholder="Date of Birth...">
	                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
	              </div>
	              <div class="form-group">
	                <label for="exampleInputEmail1">Place of Birth</label>
	                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.placeOfBirth" aria-describedby="emailHelp" placeholder="Place of Birth...">
	              </div>
	              <div class="form-group">
	                <label for="exampleInputEmail1">Religion</label>
	                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.religion" aria-describedby="emailHelp" placeholder="Religion...">
	              </div>
	              <div class="form-group">
	                <label for="exampleInputEmail1">Contact No.</label>
	                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.contact" aria-describedby="emailHelp" placeholder="Contact No...">
	              </div>
	              <div class="form-group">
	                <label for="exampleInputEmail1">E-Mail Address</label>
	                <input type="text" class="form-control" id="exampleInputEmail1" ng-model="data.email" aria-describedby="emailHelp" placeholder="E-Mail Address...">
	              </div>
	            </div>

	          </div>

          	</fieldset>


        </form>
	</div>
	<div class="modal-footer">
		<button class="btn btn-primary btn-o" ng-click="cancel()">Close</button>
	</div>
</script>





