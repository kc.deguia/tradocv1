<!DOCTYPE html>
<html lang="en" data-ng-app="clipApp">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="description" content="{[{app.description}]}">
		<meta name="keywords" content="app, responsive, angular, bootstrap, dashboard, admin">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta name="HandheldFriendly" content="true" />
		<meta name="apple-touch-fullscreen" content="yes" />
		<title data-ng-bind="pageTitle()">Clip-Two - Angular Bootstrap Admin Template</title>
		<!-- Google fonts -->
		<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
		<!-- Bootstrap -->
		<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
		<!-- Themify Icons -->
		<link rel="stylesheet" href="bower_components/themify-icons/css/themify-icons.css">
		<!-- Loading Bar -->
		<link rel="stylesheet" href="bower_components/angular-loading-bar/build/loading-bar.min.css">
		<!-- Animate Css -->
		<link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
		<!-- Clip-Two CSS -->
		<link rel="stylesheet" href="assets/css/styles.css">
		<link rel="stylesheet" href="assets/css/plugins.css">
		<!-- Clip-Two Theme -->
		<link rel="stylesheet" data-ng-href="assets/css/themes/{[{ app.layout.theme }]}.css" />

		{#-- INITTIAL_LOADED_MODULE #}
		{# //*** jQuery Pluginss #}
		<link rel="stylesheet" data-ng-href="bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
		<link rel="stylesheet" data-ng-href="bower_components/sweetalert/dist/sweetalert.css" />
		{# //*** angularJS Moduless #}
		<link rel="stylesheet" data-ng-href="bower_components/angular-ui-switch/angular-ui-switch.min.css" />
		<link rel="stylesheet" data-ng-href="bower_components/AngularJS-Toaster/toaster.css" />
		<link rel="stylesheet" data-ng-href="bower_components/angular-aside/dist/css/angular-aside.min.css" />
		<link rel="stylesheet" data-ng-href="bower_components/v-accordion/dist/v-accordion.min.css" />
		<link rel="stylesheet" data-ng-href="bower_components/angular-notification-icons/dist/angular-notification-icons.min.css" />


	</head>
	<body ng-controller="AppCtrl">

		{#<div ui-view ng-class="{'app-mobile' : app.isMobile, 'app-navbar-fixed' : app.layout.isNavbarFixed, 'app-sidebar-fixed' : app.layout.isSidebarFixed, 'app-sidebar-closed':app.layout.isSidebarClosed, 'app-footer-fixed':app.layout.isFooterFixed}">
		</div>#}

		<div  ng-class="{'app-mobile' : app.isMobile, 'app-navbar-fixed' : app.layout.isNavbarFixed, 'app-sidebar-fixed' : app.layout.isSidebarFixed, 'app-sidebar-closed':app.layout.isSidebarClosed, 'app-footer-fixed':app.layout.isFooterFixed}">

			<!-- toaster directive -->
			<toaster-container toaster-options="{'position-class': 'toast-top-right', 'close-button':true}"></toaster-container>
			<!-- / toaster directive -->
			<!-- sidebar -->
			<div class="sidebar app-aside hidden-print" id="sidebar" toggleable parent-active-class="app-slide-off" >
				<div perfect-scrollbar wheel-propagation="false" suppress-scroll-x="true" class="sidebar-container">
					<div data-ng-include=" 'assets/views/partials/sidebar.html' "></div>
				</div>
			</div>
			<!-- / sidebar -->
			<div class="app-content" ng-class="{loading: loading}">
				<!-- top navbar -->
				<header data-ng-include=" 'assets/views/partials/top-navbar.html' " class="navbar navbar-default navbar-static-top hidden-print"></header>
				<!-- / top navbar -->
				<!-- main content -->
				<div class="main-content" >
					<div ui-view class="wrap-content container fade-in-up" id="container"></div>
				</div>
				<!-- / main content -->
			</div>
			<!-- footer -->
			<footer data-ng-include=" 'assets/views/partials/footer.html' " class="hidden-print"></footer>
			<!-- / footer -->
			<div data-ng-include=" 'assets/views/partials/off-sidebar.html' " id="off-sidebar" class="sidebar hidden-print" toggleable parent-active-class="app-offsidebar-open"></div>
			<div data-ng-include=" 'assets/views/partials/settings.html' " toggleable class="settings panel panel-default hidden-xs hidden-sm hidden-print" id="settings"></div>


		</div>



		<!-- jQuery -->
		<script src="bower_components/jquery/dist/jquery.min.js"></script>
		<!-- Fastclick -->
		<script src="bower_components/fastclick/lib/fastclick.js"></script>
		<!-- Angular -->
		<script src="bower_components/angular/angular.min.js"></script>
		<script src="bower_components/angular-cookies/angular-cookies.min.js"></script>
		<script src="bower_components/angular-animate/angular-animate.min.js"></script>
		<script src="bower_components/angular-touch/angular-touch.min.js"></script>
		<script src="bower_components/angular-sanitize/angular-sanitize.min.js"></script>
		<script src="bower_components/angular-ui-router/release/angular-ui-router.js"></script>
		<!-- Angular storage -->
		<script src="bower_components/ngstorage/ngStorage.min.js"></script>
		<!-- Angular Translate -->
		<script src="bower_components/angular-translate/angular-translate.js"></script>
		<script src="bower_components/angular-translate-loader-url/angular-translate-loader-url.js"></script>
		<script src="bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js"></script>
		<script src="bower_components/angular-translate-storage-local/angular-translate-storage-local.js"></script>
		<script src="bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.js"></script>
		<!-- oclazyload -->
		<script src="bower_components/oclazyload/dist/ocLazyLoad.min.js"></script>
		<!-- breadcrumb -->
		<script src="bower_components/angular-breadcrumb/dist/angular-breadcrumb.min.js"></script>
		<!-- UI Bootstrap -->
		<script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
		<!-- Loading Bar -->
		<script src="bower_components/angular-loading-bar/build/loading-bar.min.js"></script>
		<!-- Angular Scroll -->
		<script src="bower_components/angular-scroll/angular-scroll.min.js"></script>

		<!-- LAZYLOAD-->
		<script src="bower_components/oclazyload/dist/ocLazyLoad.min.js"></script>



		{# -- INITTIAL_LOADED_MODULE  #}
		{# //*** Javascript Plugins #}
		<script src="bower_components/modernizr/modernizr.js"></script>
		<script src="bower_components/moment/min/moment.min.js"></script>
		{# //*** jQuery Plugins #}
		<script src="bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
		{#<script src="bower_components/sweetalert/dist/sweetalert.min.js"></script>#}
		<script src="bower_components/chart.js/dist/Chart.min.js"></script>
		{# *** Filters#}
		<!-- <script src="assets/js/filters/htmlToPlaintext.js"></script> -->
		{# //*** angularJS Moduless #}
		<script src="bower_components/angular-moment/angular-moment.min.js"></script>
		<script src="bower_components/angular-ui-switch/angular-ui-switch.min.js"></script>
		<script src="bower_components/AngularJS-Toaster/toaster.js"></script>
		<script src="bower_components/angular-aside/dist/js/angular-aside.min.js"></script>
		<script src="bower_components/v-accordion/dist/v-accordion.min.js"></script>
		<script src="bower_components/tc-angular-chartjs/dist/tc-angular-chartjs.min.js"></script>
		<script src="bower_components/ngSweetAlert/SweetAlert.min.js"></script>
		<script src="bower_components/angular-truncate/src/truncate.js"></script>
		<script src="bower_components/angular-notification-icons/dist/angular-notification-icons.min.js"></script>


		<!-- Clip-Two Scripts -->
		<script src="assets/js/app.js"></script>
		<script src="assets/js/main.js"></script>
		<script src="assets/js/config.constant.js"></script>
		<script src="scripts/config.router.js"></script>
		<!-- Clip-Two Directives -->
		<script src="assets/js/directives/toggle.js"></script>
		<script src="assets/js/directives/perfect-scrollbar.js"></script>
		<script src="assets/js/directives/empty-links.js"></script>
		<script src="assets/js/directives/sidebars.js"></script>
		<script src="assets/js/directives/off-click.js"></script>
		<script src="assets/js/directives/full-height.js"></script>
		<script src="assets/js/directives/panel-tools.js"></script>
		<script src="assets/js/directives/char-limit.js"></script>
		<script src="assets/js/directives/dismiss.js"></script>
		<script src="assets/js/directives/compare-to.js"></script>
		<script src="assets/js/directives/select.js"></script>
		<script src="assets/js/directives/messages.js"></script>
		<script src="assets/js/directives/chat.js"></script>
		<script src="assets/js/directives/sparkline.js"></script>
		<script src="assets/js/directives/touchspin.js"></script>
		<script src="assets/js/directives/file-upload.js"></script>
		<!-- Clip-Two Controllers -->
		<script src="assets/js/controllers/mainCtrl.js"></script>
		<script src="assets/js/controllers/inboxCtrl.js"></script>
		<script src="assets/js/controllers/bootstrapCtrl.js"></script>

		<script src="assets/js/controllers/chatCtrl.js"></script>
		<script src="scripts/factory/register/regFctry.js"></script>

	</body>
</html>
