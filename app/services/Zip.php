<?php 

namespace Services;

class Zip {
    private $_zip;
    private $_temp;
    private $_s3;
    private $_filename;
    private $_files;

    public function __construct($s3, $filename) {
        $this->_s3 = $s3;

        if(!file_exists(__DIR__ . '/../cache/documents')) {
            mkdir(__DIR__ . '/../cache/documents');
        }

        $this->filename = $filename;
        $this->_zip = new \ZipArchive;
        $this->_temp = tempnam(sys_get_temp_dir(), 'zip');
        $this->_zip->open($this->_temp);
    }

    private function downloadFromS3($file) {
        $s3 = new \S3($this->_s3['awsAccessKey'], $this->_s3['awsSecretKey']);
        $uploadName = ($file->directory . $file->storedFileName);
        $res = \S3::getObject($this->_s3['bucket'], $uploadName, __DIR__ . '/../cache/documents/' . $file->storedFileName);
    }

    public function includeFiles($files) {
        $this->_files = $files;
        foreach ($files as $file) {
            if(!file_exists(__DIR__ . '/../cache/documents/' . $file->storedFileName)) {
                $this->downloadFromS3($file);
            }

            $this->_zip->addFile( __DIR__ . '/../cache/documents/' . $file->storedFileName, $file->fileName);
        }
    }

    private function cleanCached() {
        foreach($this->_files as $key => $value) {
            if(!unlink(__DIR__ . '/../cache/documents/' . $value->storedFileName)) {
                die();
            }
        }
    }

    public function export() {
        $this->_zip->close();

        $this->cleanCached();

        // Stream file to browser
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $this->filename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($this->_temp));

        readfile($this->_temp);

        unlink($this->_temp);

        exit;
    }
}