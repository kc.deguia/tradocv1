<?php 

namespace Services;

class ResourceHandler {
	
	public $url;
	public $method;
	public $fields = array();

	public function __construct($url, $method) {
		$this->url = $url;
		$this->method = $method;
	}

	public function setFields($fields) {
		$this->fields = $fields;
	}

	public function get() {
		$curl 	= curl_init();
		$params = http_build_query($this->fields);

		curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, '/windows nt 6.2/i');

        if(strtoupper($this->method) == 'POST') {
			curl_setopt($curl, CURLOPT_POST, count($this->fields));
			curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        }

		$response = curl_exec($curl);

		if (!$response) {
            $info = curl_getinfo($curl);
            curl_close($curl);
        }

		curl_close($curl);
        return json_decode($response);
	}

	public function toArray() {
		return (array) $this;
	}

}